const { serverFactory } = require("./server");

serverFactory()
    .then(() => {
        console.log("Server started!");
    })
    .catch(e => {
        console.error("Error", e.message);
    });
