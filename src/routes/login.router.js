const login = require("express").Router();

// Controlers
const { makeLogin } = require("../controllers/login.controller");

const { createLoginValidator } = require("../validators/login.validator");

// Routers products
login.post("/", createLoginValidator(), makeLogin);

module.exports = login;
