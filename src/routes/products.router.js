const products = require("express").Router();

// Controlers
const {
    getAll,
    getById,
    insertDatabase
} = require("../controllers/products.controller");

// Routers products
products.get("/", getAll);
products.get("/insertdatabase/", insertDatabase);
products.get("/:id", getById);

module.exports = products;
