const routes = require("express").Router();
const clients = require("./clients.router");
const products = require("./products.router");
const login = require("./login.router");

routes.use("/clients", clients);
routes.use("/products", products);
routes.use("/login", login);

module.exports = routes;
