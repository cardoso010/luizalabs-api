const clients = require("express").Router();

// Controlers
const {
    getAll,
    getById,
    create,
    update,
    remove,
    getAllFavoriteProductsByClient,
    createFavoriteProducts
} = require("../controllers/clients.controller");

// Validators
const { createClientsValidator } = require("../validators/clients.validator");
const {
    createClientAndProductValidator
} = require("../validators/favorite_products.validator");

// Routers clients
clients.get("/", getAll);
clients.get("/:id", getById);
clients.post("/", createClientsValidator(), create);
clients.put("/:id", update);
clients.delete("/:id", remove);

// Routers favorite products
clients.get("/:id/favoriteproducts", getAllFavoriteProductsByClient);
clients.post(
    "/:id/favoriteproducts",
    createClientAndProductValidator(),
    createFavoriteProducts
);

module.exports = clients;
