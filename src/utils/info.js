/**
 * show basic information about application
 */
const showInfo = () => {
    const infos = {
        NAME: { value: process.env.APP_NAME },
        HOST: { value: "localhost" },
        PORT: { value: process.env.PORT },
        ENV: {
            value:
                process.env.NODE_ENV === "production"
                    ? "production"
                    : "development"
        },
        SWAGGER: {
            value: `http://localhost:${process.env.PORT}/api-docs/`
        },
        STATUS: {
            value: `http://localhost:${process.env.PORT}/status`
        }
    };
    console.table(infos);
};

module.exports = { showInfo };
