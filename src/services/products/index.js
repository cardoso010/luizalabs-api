const axios = require("axios");
const { isEmpty } = require("lodash");
const business = require("../../business/products.business");

/**
 * Insert inside database.
 */
const insertProducts = async data => {
    try {
        data.forEach(product => {
            business.create(product);
        });
    } catch (error) {
        console.log("Error insertProducts -> ", error);
    }
};

/**
 * Get products to insert inside database.
 */
const getProducts = async () => {
    try {
        const url = process.env.API_PRODUCT;
        if (!isEmpty(url)) {
            [1, 2, 3].forEach(async page => {
                const response = await axios.get(url, {
                    params: {
                        page
                    }
                });
                insertProducts(response.data.products);
            });
        }
    } catch (error) {
        console.log("Error getProducts -> ", error);
    }
};

module.exports = {
    getProducts
};
