const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { isEmpty } = require("lodash");
const repository = require("../../repositories/users.repositories");

/**
 * Check if it's the same.
 */
const checkPasswords = async (data, encripted) => {
    const isMatch = await bcrypt.compare(data, encripted);
    if (isMatch) {
        return;
    }

    throw new Error("Senha invalida!");
};

/**
 * Check if it's the same.
 */
const makeAuth = async data => {
    const user = await repository.getByEmail(data.email);
    if (isEmpty(user)) {
        throw new Error("Usuário nao existe!");
    }
    await checkPasswords(data.password, user.password);

    return user;
};

/**
 * Function for to do a token jwt.
 */
const makeToken = payload => {
    return jwt.sign(payload, process.env.APP_KEY, {
        expiresIn: "5h"
    });
};

module.exports = {
    checkPasswords,
    makeAuth,
    makeToken
};
