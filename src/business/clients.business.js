const repository = require("../repositories/clients.repositories");

/**
 * Create a new client.
 */
const create = async payload => {
    let result = null;
    const { email } = payload;
    const client = await repository.getByEmail(email);
    if (client) {
        result = { error: "Email ja foi cadastrado!" };
    } else {
        result = repository.create(payload);
    }

    return result;
};

/**
 * Get all clients.
 */
const getAll = () => {
    return repository.getAll();
};

/**
 * Get specific clients.
 */
const getById = id => {
    return repository.getById(id);
};

/**
 * Update a client.
 */
const update = (id, payload) => {
    const client = repository.update(id, payload);
    return client;
};

/**
 * Remove a client.
 */
const remove = id => {
    return repository.remove(id);
};

module.exports = {
    getAll,
    getById,
    create,
    update,
    remove
};
