const { isEmpty } = require("lodash");
const repository = require("../repositories/favorite_products.repositories");

/**
 * Create a new favorite products.
 */
const create = async payload => {
    let result = null;
    const favoriteProduct = await repository.getByIdClientAndIdProduct(payload);
    if (isEmpty(favoriteProduct)) {
        result = repository.create(payload);
    } else {
        result = { error: "O cliente já possui esse produto favorito!" };
    }

    return result;
};

/**
 * Get all favorite products.
 */
const getAll = () => {
    return repository.getAll();
};

/**
 * Get by clients.
 */
const getByIdClient = id => {
    return repository.getByIdClient(id);
};

module.exports = {
    getAll,
    getByIdClient,
    create
};
