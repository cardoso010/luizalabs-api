const repository = require("../repositories/products.repositories");

/**
 * Create a new product.
 */
const create = async payload => {
    return repository.create(payload);
};

/**
 * Get all products.
 */
const getAll = page => {
    return repository.getAll(page);
};

/**
 * Get specific product.
 */
const getById = id => {
    return repository.getById(id);
};

module.exports = {
    getAll,
    getById,
    create
};
