/**
 * Module dependencies.
 */
const express = require("express");
const compression = require("compression");
const expressStatusMonitor = require("express-status-monitor");
const bodyParser = require("body-parser");
const errorHandler = require("errorhandler");
const cors = require("cors");
const morgan = require("morgan");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("../../swagger.json");
const { getConfig } = require("../utils/config");
const { showInfo } = require("../utils/info");
const routes = require("../routes");

const serverFactory = async () => {
    const config = getConfig();
    /**
     * Create Express server.
     */
    const server = express();

    /**
     * Express configuration.
     */
    server.set("port", process.env.PORT || 3004);
    server.use(expressStatusMonitor());
    server.use(compression());
    server.use(bodyParser.json());
    server.use(bodyParser.urlencoded({ extended: true }));
    server.use(cors());
    server.use("/", routes);
    server.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

    server.use(morgan("dev"));

    /**
     * Error Handler.
     */
    if (process.env.NODE_ENV === "development") {
        // only use in development
        server.use(errorHandler());
    } else {
        server.use((err, req, res, next) => {
            console.error(err);
            res.status(500).send("Server Error");
        });
    }

    showInfo();

    /**
     * Start Express server.
     */
    server.listen(server.get("port"));

    return { server, config };
};

module.exports = { serverFactory };
