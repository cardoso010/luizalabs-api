const uuid = require("uuid");
const { head } = require("lodash");
const db = require("../services/database/");

/**
 * Get all clients.
 */
const getAll = async () => {
    const clients = await db("clients");
    return clients;
};

/**
 * Get specific clients.
 */
const getById = async id => {
    const client = await db("clients")
        .where({ id })
        .first();
    return client;
};

/**
 * Get specific clients by email.
 */
const getByEmail = async email => {
    const client = await db("clients")
        .where({ email })
        .first();
    return client;
};

/**
 * Create a new client.
 */
const create = async payload => {
    const client = await db("clients")
        .returning("*")
        .insert({
            id: uuid(),
            ...payload
        })
        .then(head);
    return client;
};

/**
 * Update a client.
 */
const update = async (id, payload) => {
    const { name, email } = payload;
    const client = await db("clients")
        .where({ id })
        .update({
            name,
            email
        })
        .then(() => {
            return db("clients")
                .where({ id })
                .first();
        });
    return client;
};

/**
 * Remove a client.
 */
const remove = async id => {
    const client = await db("clients")
        .where({ id })
        .del();
    return client;
};

module.exports = {
    getAll,
    getById,
    getByEmail,
    create,
    update,
    remove
};
