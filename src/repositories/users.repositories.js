const uuid = require("uuid");
const { head } = require("lodash");
const db = require("../services/database/");

/**
 * Get all users.
 */
const getAll = async () => {
    const users = await db("users");
    return users;
};

/**
 * Get specific users.
 */
const getById = async id => {
    const client = await db("users")
        .where({ id })
        .first();
    return client;
};

/**
 * Get specific users by email.
 */
const getByEmail = async email => {
    const client = await db("users")
        .where({ email })
        .first();
    return client;
};

/**
 * Create a new client.
 */
const create = async payload => {
    const client = await db("users")
        .returning("*")
        .insert({
            id: uuid(),
            ...payload
        })
        .then(head);
    return client;
};

/**
 * Update a users.
 */
const update = async (id, payload) => {
    const { name, email } = payload;
    const client = await db("users")
        .where({ id })
        .update({
            name,
            email
        })
        .then(() => {
            return db("users")
                .where({ id })
                .first();
        });
    return client;
};

/**
 * Remove a users.
 */
const remove = async id => {
    const client = await db("users")
        .where({ id })
        .del();
    return client;
};

module.exports = {
    getAll,
    getById,
    getByEmail,
    create,
    update,
    remove
};
