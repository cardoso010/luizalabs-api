const { head } = require("lodash");
const db = require("../services/database/");

const table = "favorite_products";

/**
 * Get all favorite products.
 */
const getAll = async () => {
    const favoriteProducts = await db(table);
    return favoriteProducts;
};

/**
 * Get by client.
 */
const getByIdClient = async id => {
    const favoriteProducts = await db(table)
        .select()
        .join("products", `${table}.product_id`, "=", "products.id")
        .where({
            client_id: id
        });
    return favoriteProducts;
};

/**
 * Get by client and product.
 */
const getByIdClientAndIdProduct = async payload => {
    const favoriteProducts = await db(table).where({
        ...payload
    });
    return favoriteProducts;
};

/**
 * Create a new favorite products.
 */
const create = async payload => {
    const favoriteProduct = await db(table)
        .returning("*")
        .insert({
            ...payload
        })
        .then(head);
    return favoriteProduct;
};

module.exports = {
    getAll,
    getByIdClient,
    getByIdClientAndIdProduct,
    create
};
