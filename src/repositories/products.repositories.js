const { head } = require("lodash");
const setupPaginator = require("knex-paginator");
const db = require("../services/database/");

setupPaginator(db);

const table = "products";
/**
 * Get all products.
 */
const getAll = async page => {
    const products = await db(table).paginate(100, page, true);
    return products;
};

/**
 * Get specific products.
 */
const getById = async id => {
    const product = await db(table)
        .where({ id })
        .first();
    return product;
};

/**
 * Create a new product.
 */
const create = async payload => {
    const product = await db(table)
        .returning("*")
        .insert({
            ...payload
        })
        .then(head);
    return product;
};

module.exports = {
    getAll,
    getById,
    create
};
