const { check } = require("express-validator");
const { isEmpty } = require("lodash");
const productBusiness = require("../business/products.business");

const createClientAndProductValidator = () => {
    return [
        check("product_id", "Id do produto é obrigatorio!")
            .exists()
            .custom(async value => {
                const product = await productBusiness.getById(value);
                if (isEmpty(product)) {
                    throw new Error("Produto não encontrado!");
                }

                return true;
            })
    ];
};

module.exports = {
    createClientAndProductValidator
};
