const { check } = require("express-validator");

const createLoginValidator = () => {
    return [
        check("email", "Email é obrigatorio!").exists(),
        check("password", "Senha é obrigatorio!").exists()
    ];
};

module.exports = {
    createLoginValidator
};
