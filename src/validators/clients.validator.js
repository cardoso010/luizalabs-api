const { check } = require("express-validator");

const createClientsValidator = () => {
    return [
        check("name", "Nome é obrigatorio!").exists(),
        check("email", "Email é obrigatorio!").exists(),
        check("email", "Email invalido!").isEmail()
    ];
};

module.exports = {
    createClientsValidator
};
