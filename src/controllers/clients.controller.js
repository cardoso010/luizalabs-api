const { validationResult } = require("express-validator");
const { isEmpty } = require("lodash");
const business = require("../business/clients.business");
const businessFavoriteProducts = require("../business/favorite_products.business");

/**
 * GET /clients/
 * Get all clients.
 */
const getAll = async (req, res) => {
    const allClients = await business.getAll();
    res.status(200).json(allClients);
};

/**
 * GET /clients/:id
 * Get specific clients.
 */
const getById = async (req, res) => {
    const { id } = req.params;
    const client = await business.getById(id);
    if (isEmpty(client)) {
        res.status(404).json({ error: "Cliente nao encontrado!" });
    }
    res.status(200).json(client);
};

/**
 * POST /clients/
 * Create a new client.
 */
const create = async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    const client = await business.create(req.body);

    res.status(200).json(client);
};

/**
 * PUT /clients/:id
 * Update a client.
 */
const update = async (req, res) => {
    const { id } = req.params;
    const client = await business.update(id, req.body);
    res.status(200).json(client);
};

/**
 * REMOVE /clients/:id
 * Remove a client.
 */
const remove = async (req, res) => {
    const { id } = req.params;
    const client = await business.remove(id);
    if (!client) {
        res.status(404).json({ error: "Cliente nao encontrado!" });
    }
    res.status(200).json({ msg: "Cliente excluido com sucesso!" });
};

/**
 * GET /clients/:id/favoriteproducts/
 * Get all favorite products of the client.
 */
const getAllFavoriteProductsByClient = async (req, res) => {
    const { id } = req.params;
    const allClients = await businessFavoriteProducts.getByIdClient(id);
    res.status(200).json(allClients);
};

/**
 * POST /clients/:id/favoriteproducts/
 * Create a new favorite products of the client.
 */
const createFavoriteProducts = async (req, res) => {
    const data = { client_id: req.params.id, product_id: req.body.product_id };
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array()[0] });
        return;
    }

    const client = await businessFavoriteProducts.create(data);

    res.status(200).json(client);
};

module.exports = {
    getAll,
    getById,
    create,
    update,
    remove,
    getAllFavoriteProductsByClient,
    createFavoriteProducts
};
