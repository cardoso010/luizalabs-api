const { isEmpty } = require("lodash");
const business = require("../business/products.business");
const services = require("../services/products");

/**
 * GET /products/?page=<Page>
 * Get all products by pagination.
 */
const getAll = async (req, res) => {
    const { page } = req.query;
    const allClients = await business.getAll(page);
    res.status(200).json(allClients);
};

/**
 * GET /products/:id
 * Get specific products.
 */
const getById = async (req, res) => {
    const { id } = req.params;
    const product = await business.getById(id);
    if (isEmpty(product)) {
        res.status(404).json({ error: "Produto nao encontrado!" });
    }
    res.status(200).json(product);
};

/**
 * GET /products/insertdatabase
 * Insert data inside products table.
 */
const insertDatabase = async (req, res) => {
    try {
        services.getProducts();
        res.status(200).end();
    } catch (error) {
        res.status(500).json({ error: "Erro ao executar o servico!" });
    }
};

module.exports = {
    getAll,
    getById,
    insertDatabase
};
