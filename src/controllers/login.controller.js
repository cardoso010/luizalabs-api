const services = require("../services/auth");

/**
 * POST /login/
 * Make a login.
 */
const makeLogin = async (req, res) => {
    try {
        const user = await services.makeAuth(req.body);
        const token = services.makeToken(user);
        res.status(200).json({ token });
    } catch (error) {
        res.status(404).json({ error: error.message });
    }
};

module.exports = {
    makeLogin
};
