# Desafio Luiza Labs

## Requisitos

```
- node
- yarn
- docker
- docker-compose
```

## Getting Started

Clonar repositório

```
$git clone git@bitbucket.org:cardoso010/luizalabs-api.git
```

Acessar o diretorio e criar o .env

```
$cd luizalabs-api/
```

Copiar o .env

```
$cp .env.example .env
```

Criação dos conteiners e execução do projeto

```
$yarn run docker:dev
```

O projeto irá rodar na seguinte url e porta

```
http://localhost:3003/
```

A documentação da api está rodando na seguinte url

```
http://localhost:3003/api-docs/
```

#### pgadmin4

Para acessar de forma mais facil o banco de dados basta acessar a url `http://localhost:16543` usando o usuario `local@local.dev` e a senha `desafioapi123` que foi definida no .env.
É necessario criação de um server e adicionar os dados de acesso ao banco que está no .env.

### Status servidor

Para verificar o status do servidor como a quantidade de cpu e memoria utilizada basta acessar o seguinte link após a execução do projeto.

```
http://localhost:3003/status
```

## Autor

-   **Gabriel Cardoso Luiz** - _Initial work_ - [cardoso010](https://github.com/cardoso010)
