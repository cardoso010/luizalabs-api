const uuid = require("uuid");
const bcrypt = require("bcrypt");

exports.seed = async knex => {
    // Inserts seed entries
    const passwords = "123456";
    const password = await bcrypt.hash(passwords, 5);
    // Deletes ALL existing entries
    await knex("users").del();

    return knex("users").insert([
        {
            id: uuid(),
            name: "Admin",
            email: "admin@admin.com",
            password
        },
        {
            id: uuid(),
            name: "Desafio",
            email: "desafio@luizalabs.com",
            password
        }
    ]);
};
