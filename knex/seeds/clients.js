const uuid = require("uuid");

exports.seed = knex => {
    // Deletes ALL existing entries
    return knex("clients")
        .del()
        .then(() => {
            // Inserts seed entries
            return knex("clients").insert([
                { id: uuid(), name: "Gabriel", email: "gabriel@cardoso.com" },
                { id: uuid(), name: "Desafio", email: "desafio@luizalabs.com" }
            ]);
        });
};
