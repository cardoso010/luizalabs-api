exports.up = knex => {
    return knex.schema.createTable("products", table => {
        table.uuid("id").primary();
        table.string("title").notNullable();
        table.string("brand").notNullable();
        table.string("image").notNullable();
        table.decimal("price", 8, 2).notNullable();
        table.decimal("reviewScore", 8, 2);
        table.timestamps(true, true);
    });
};

exports.down = knex => {
    return knex.schema.dropTable("products");
};
