exports.up = knex => {
    return knex.schema.createTable("clients", table => {
        table.uuid("id").primary();
        table.string("name").notNullable();
        table
            .string("email")
            .unique()
            .notNullable();
        table.timestamps(true, true);
    });
};

exports.down = knex => {
    return knex.schema.dropTable("clients");
};
