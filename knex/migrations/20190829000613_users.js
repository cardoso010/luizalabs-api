exports.up = knex => {
    return knex.schema.createTable("users", table => {
        table.uuid("id").primary();
        table.string("name").notNullable();
        table
            .string("email")
            .unique()
            .notNullable();
        table.string("password");
        table.timestamps(true, true);
    });
};

exports.down = knex => {
    return knex.schema.dropTable("users");
};
