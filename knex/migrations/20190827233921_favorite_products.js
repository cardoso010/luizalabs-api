exports.up = knex => {
    return knex.schema.createTable("favorite_products", table => {
        table.uuid("client_id").notNullable();
        table.uuid("product_id").notNullable();
        table.timestamps(true, true);

        table
            .foreign("client_id")
            .references("id")
            .on("clients");

        table
            .foreign("product_id")
            .references("id")
            .on("products");
    });
};

exports.down = knex => {
    return knex.schema.dropTable("favorite_products");
};
