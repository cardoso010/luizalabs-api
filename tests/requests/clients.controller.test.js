const request = require("supertest");
const { serverFactory } = require("../../src/server");

serverFactory().then(({ server }) => {
    describe("GET /clients", () => {
        it("should return 200 OK", done => {
            request(server)
                .get("/clients")
                .expect(200, done);
        });
    });
});
