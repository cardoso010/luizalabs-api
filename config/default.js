const path = require("path");

const envConfig = require("dotenv").config();

for (const k in envConfig) {
    process.env[k] = envConfig[k];
}

module.exports = {
    SERVER_NAME: "desafio-api",
    SERVER_BASE_DIR: path.join(__dirname, "../src"),
    PORT: process.env.PORT,
    NODE_ENV: process.env.NODE_ENV,
    APP_KEY: process.env.APP_KEY || "no key",
    services: {
        database: {
            client: "pg",
            connection: {
                host: process.env.DB_HOST || "portgres",
                database: process.env.DB_NAME || "desafio-api",
                user: process.env.DB_USER,
                password: process.env.DB_PASSWORD
            }
        }
    },
    env: {
        DEBUG_COLORS: true
    }
};
