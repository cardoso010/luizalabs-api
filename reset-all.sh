echo "------- Apagando node_modules -------"
rm -rf node_modules
echo "------- node_modules APAGADO -------"

echo "------- Apagando data/db -------"
rm -rf data/db
echo "------- data/db APAGADO -------"

docker ps -a
echo "------- Apagando containers -------"
docker rm -f desafio-api-server
docker rm -f desafio-api-pgadmin4
docker rm -f desafio-api-postgres
echo "------- containers APAGADO -------"

docker ps -a
echo "------- Finalizado -------"
